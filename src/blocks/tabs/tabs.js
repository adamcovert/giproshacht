var ready = function ( fn ) {

  // Sanity check
  if ( typeof fn !== 'function' ) return;

  // If document is already loaded, run method
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }

  // Otherwise, wait until document is loaded
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

ready(function () {
  'use-strict';

  /**
   * Element.closest() polyfill
   * https://developer.mozilla.org/en-US/docs/Web/API/Element/closest#Polyfill
   */
  if (!Element.prototype.closest) {
    if (!Element.prototype.matches) {
      Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
    }
    Element.prototype.closest = function (s) {
      var el = this;
      var ancestor = this;
      if (!document.documentElement.contains(el)) return null;
      do {
        if (ancestor.matches(s)) return ancestor;
        ancestor = ancestor.parentElement;
      } while (ancestor !== null);
      return null;
    };
  }

  if (location.hash) {
    showTab(location.hash);
  }

  document.addEventListener('click', function (event) {

    if (event.target.dataset.toggle === 'tab') {

      // Prevent default link behavior
      event.preventDefault();

      var target = event.target.hash === undefined ? event.target.dataset.target : event.target.hash;
      if (target !== undefined) {
        showTab(target);

        if (history && history.pushState && history.replaceState) {
          var stateObject = {'url' : target};
          if (window.location.hash && stateObject.url !== window.location.hash) {
            window.history.pushState(stateObject, document.title, window.location.pathname + target);
          } else {
            window.history.replaceState(stateObject, document.title, window.location.pathname + target);
          }
        }
      }
    }
  });

  // Show tab content
  function showTab(tabId){
    var element = document.querySelector(tabId);

    if (element && element.classList.contains('tabs__content-item')) {
      var tabsParent = document.querySelector(tabId).closest('.tabs');
      var activeTabClassName = 'tabs__link-wrap--active';
      var activeTabContentClassName = 'tabs__content-item--active';

      // Get all tab links, loop through it, and remove active class
      tabsParent.querySelectorAll('.' + activeTabClassName).forEach( function(item) {
        item.classList.remove(activeTabClassName);
      });

      var activeTab = tabsParent.querySelector('[href="'+tabId+'"]') ? tabsParent.querySelector('[href="'+tabId+'"]') : tabsParent.querySelector('[data-target="'+tabId+'"]')
      activeTab.closest('.tabs__link-wrap').classList.add(activeTabClassName);

      // Get all tab content, loop through it, and remove active class
      tabsParent.querySelectorAll('.'+activeTabContentClassName).forEach(function(item){
        item.classList.remove(activeTabContentClassName);
      });

      // Open content
      tabsParent.querySelector(tabId).classList.add(activeTabContentClassName);
    }
  };
});
