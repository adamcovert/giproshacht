$(document).ready(function () {

  $('#our-mission-slider').owlCarousel({
    items: 1,
    dots: true,
    margin: 32
  });

  $('#key-directions-slider').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    navText: ['<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>', '<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>'],
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      992: {
        items: 3
      },
      1200: {
        items: 4
      }
    }
  });

  $('#promo-team-slider').owlCarousel({
    navText: ['<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>', '<svg width="50px" height="20px" viewBox="0 0 50 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 45,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 37,2 45,10 37,18 " /></g></svg>'],
    responsive: {
      0: {
        items: 1,
        stagePadding: 32,
        margin: 22,
        dots: false,
        nav: true,
      },
      375: {
        items: 1,
        stagePadding: 72,
        margin: 32,
        dots: false,
        nav: true,
      },
      425: {
        items: 1,
        stagePadding: 92,
        margin: 32,
        dots: false,
        nav: true,
      },
      768: {
        items: 3,
        stagePadding: 52,
        margin: 32,
        dots: false,
        nav: true,
      },
      992: {
        items: 2,
        stagePadding: 12,
        margin: 32,
        dots: false,
        nav: true
      },
      1200: {
        items: 3,
        stagePadding: 16,
        margin: 24,
        dots: false,
        nav: true
      }
    }
  });

  $('#promo-news-slider').owlCarousel({
    slideBy: 1,
    responsive: {
      0: {
        items: 1,
        stagePadding: 32,
        margin: 22,
        loop: true,
        dots: true
      },
      375: {
        items: 1,
        stagePadding: 52,
        margin: 32,
        loop: true,
        dots: true
      },
      768: {
        items: 2,
        stagePadding: 52,
        margin: 32,
        loop: true,
        dots: true
      },
      1024: {
        items: 4,
        stagePadding: 20,
        margin: 16,
        mouseDrag: false,
        dots: false
      },
      1200: {
        items: 4,
        stagePadding: 20,
        margin: 32,
        mouseDrag: false,
        dots: false
      }
    }
  });

  $('#promo-testimonials-slider').owlCarousel({
    responsive: {
      0: {
        items: 1,
        dots: true
      },
      768: {
        items: 2,
        dots: true
      }
    }
  });

  $('#ar-slider').owlCarousel({
    responsive: {
      0: {
        items: 1,
        stagePadding: 40,
        margin: 10
      },
      768: {
        items: 2,
        stagePadding: 40,
        margin: 10
      },
      992: {
        items: 3,
        stagePadding: 40,
        margin: 10,
        center: true,
        loop: false
      }
    }
  });
});
