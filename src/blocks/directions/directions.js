var ready = function ( fn ) {

  // Sanity check
  if ( typeof fn !== 'function' ) return;

  // If document is already loaded, run method
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }

  // Otherwise, wait until document is loaded
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

ready(function () {
  'use-strict';

  /**
   * NodeList.prototype.forEach() polyfill
   * https://developer.mozilla.org/en-US/docs/Web/API/NodeList/forEach#Polyfill
   */
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }

  // Find all direction links
  var toggles = document.querySelectorAll('.directions__toggle');

  // Find selection element
  var selection = document.querySelector('.directions__selection');

  // Listen for clicks on the document
  document.addEventListener('click', function (event) {

    // Bail if our clicked element doesn't have the .directions__toggle class
    if (!event.target.classList.contains('directions__toggle')) return;

    // Get the target content
    var content = document.querySelector(event.target.hash);
    if (!content) return;

    // Prevent default link behavior
    event.preventDefault();

    // Get all open directions content, loop through it, and close it
    var directions = document.querySelectorAll('.directions__content.directions__content--is-active');
    directions.forEach(function (direction) {
      direction.classList.remove('directions__content--is-active');
    });

    // Get all toggle links, loop through it, and remove active class
    toggles.forEach(function (toggle, index) {
      toggle.classList.remove('directions__toggle--is-active');
    });

    // Open our content
    content.classList.add('directions__content--is-active');

    // Create active link
    event.target.classList.add('directions__toggle--is-active');
  }, false);

  // Loop through direction links and if clicked, animate selection element
  toggles.forEach(function (toggle, index) {
    toggle.addEventListener('click', function () {
      selection.style.transform = 'translateY(' + index + '00%)';
    });
  });
});